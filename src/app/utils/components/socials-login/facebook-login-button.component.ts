import { Component, Output, EventEmitter } from '@angular/core';
import { AuthService } from '../../../shared/services';
import { Router } from '@angular/router';
import { AuthService as SocialLoginService } from 'angularx-social-login';
import { FacebookLoginProvider } from 'angularx-social-login';
import { ToastyService } from 'ng2-toasty';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'facebook-login',
  template: '<button class="fb btn" (click)="signInWithFacebook()"><i class="fa fa-facebook fa-fw"></i> Connect with Facebook</button>',
  styles: [`
  .btn {
    width: 100%;
    padding: 12px;
    border: none;
    border-radius: 4px;
    margin: 5px 0;
    opacity: 0.85;
    display: inline-block;
    font-size: 17px;
    line-height: 20px;
    text-decoration: none; /* remove underline from anchors */
  }
  .btn:hover {
    opacity: 1;
  }
  
  /* add appropriate colors to fb, twitter and google buttons */
  .fb {
    background-color: #3B5998;
    color: white;
  }
  `]
})
export class FacebookLoginButtonComponent {
  private Auth: AuthService;
  @Output() onConnected = new EventEmitter();

  constructor(private translate: TranslateService, private router: Router, auth: AuthService, private socialAuthService: SocialLoginService, private toasty: ToastyService) {
    this.Auth = auth;
  }

  signInWithFacebook(): void {
    this.socialAuthService.signIn(FacebookLoginProvider.PROVIDER_ID)
      .then((resp) => {
        this.Auth.connectFacebook(resp.authToken)
          .then(() => {
            this.toasty.success('Your Facebook account has been connected!');
            this.onConnected.emit({ platform: 'facebook', success: true }), () => (null);
          })
      })
      .catch(err => this.toasty.error(this.translate.instant('Something went wrong, please try again.')));
  }
}
