import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ViewComponent } from './components/view/view.component';
import { ListComponent } from './components/list/list.component';
import { CreateComponent } from './components/create/create.component';

const routes: Routes = [
  {
    path: 'list',
    component: ListComponent,
    data: {
      title: 'Support Manager',
      urls: [{ title: 'Support', url: '/seller-support/list' }]
    }
  },
  {
    path: 'create',
    component: CreateComponent,
    data: {
      title: 'Open New Ticket',
      urls: [{ title: 'Support', url: '/seller-support/list' }, { title: 'New Ticket', url: ' / seller-support / create' }]
    }
  },
  {
    path: 'view/:id',
    component: ViewComponent,
    data: {
      title: 'Ticket Details',
      urls: [{ title: 'Support', url: '/seller-support/list' }, { title: 'Ticket Detail', url: ' / seller-support / view /: id' }]
    }
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SupportRoutingModule { }
