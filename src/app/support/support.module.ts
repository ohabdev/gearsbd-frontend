import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { SupportRoutingModule } from './support.routing';
import { ViewComponent } from './components/view/view.component';
import { ListComponent } from './components/list/list.component';
import { CreateComponent } from './components/create/create.component';
import { SupportService } from './service/support.service';
import { OrderService } from '../order/services/order.service'

@NgModule({
  imports: [
    CommonModule,
    SupportRoutingModule,
    FormsModule,
    NgSelectModule,
    NgbModule.forRoot(),
  ],
  declarations: [ViewComponent, ListComponent, CreateComponent],
  providers: [
    SupportService,
    OrderService
  ],
  exports: []
})
export class SupportModule { }
