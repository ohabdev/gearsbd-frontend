import { Component, OnInit } from '@angular/core';
import { SupportService } from '../../service/support.service';
import { UserService } from './../../../user/user.service';
import { OrderService } from '../../../order/services/order.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastyService } from 'ng2-toasty';
import * as _ from 'lodash';

@Component({
  selector: 'support-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {

  public ticket : any = {
    'subject' : '',
    'category' : null,
    'reference' : null,
    'priority' : null,
    'description' : '',
  };

  public page: Number = 1;
  public take: Number = 10;
  public total: Number = 0;
  public searchFields: any = {
    status: '',
    trackingCode: ''
  };

  public user: any = {};

  public supportCategoryList = [
    {'name' : 'Order'},
    {'name' : 'Customer'},
    {'name' : 'Product upload'},
    {'name' : 'New Category creation'},
    {'name' : 'Payment/settlement issue'},
  ];

  public orderList = [
    { 'number' : 'OrderNumber', '_id' : 'OrderID' },
    { 'number' : 'OrderNumber', '_id' : 'OrderID' },
    { 'number' : 'OrderNumber', '_id' : 'OrderID' },
    { 'number' : 'OrderNumber', '_id' : 'OrderID' },
    { 'number' : 'OrderNumber', '_id' : 'OrderID' },
  ];

  public priorityList = [
    {'name' : 'Medium'},
    {'name' : 'High'},
    {'name' : 'Low'},
  ];

  public isSubmitted: any = false;

  constructor(private router: Router, private route: ActivatedRoute,
    private supportService: SupportService, private toasty: ToastyService, private orderService: OrderService, private userService: UserService) { }

  ngOnInit() {
    this.query();
  }

  query(){
    this.userService.me().then(resp => {
      this.user = resp.data;
    });

    let params = Object.assign({
      page: this.page,
      take: this.take,
    }, this.searchFields);

    this.orderService.find(params).then((res) => {
      this.orderList = res.data.items;
      this.total = res.data.count;
    }).catch(() => this.toasty.error('Something went wrong, please try again!'));
  }

  submit(frm: any) {
    this.isSubmitted = true;
    if (frm.invalid) {
      return this.toasty.error('Form is invalid, please try again.');
    }
    this.supportService.create(this.ticket)
      .then(() => {
        this.toasty.success('Ticket has been created');
        this.router.navigate(['/seller-support/list']);
      }, err => this.toasty.error(err.data.message || 'Something went wrong!'));
  }

  // searchOrder(this, term){
  //   console.log(this);
  //   // this.searchFields.trackingCode = term;
  //   // let params = Object.assign({
  //   //   page: 1,
  //   //   take: 15,
  //   // }, {trackingCode : term});

  //   // this.orderService.find(params).then((res) => {
  //   //   this.orderList = res.data.items;
  //   //   this.total = res.data.count;
  //   // }).catch(() => this.toasty.error('Something went wrong, please try again!'));
  // }

  // searchOrder = (term: string, item: any) => {
  //   // console.log(this.searchFields);
  //   // return true;

  //   this.searchFields.trackingCode = term;
  //   let params = Object.assign({
  //     page: 1,
  //     take: 15,
  //   }, this.searchFields);

  //   this.orderService.find(params).then((res) => {
  //     this.orderList = res.data.items;
  //     this.total = res.data.count;
  //   }).catch(() => this.toasty.error('Something went wrong, please try again!'));

  //   return this.orderList;
  // }

  clearSearch(){
    this.searchFields.trackingCode = '';
    this.query();
  }

}
