import { Injectable } from '@angular/core';
import { RestangularModule, Restangular } from 'ngx-restangular';
import 'rxjs/add/operator/toPromise';

@Injectable()

export class SettlementService {

  constructor(private restangular: Restangular) { }

  getBalance(): Promise<any> {
    // return this.restangular.one('payout/balance').get().toPromise();
    return this.restangular.one('settlement/balance').get().toPromise();
  }

  payOutRequest(param): Promise<any> {
    // return this.restangular.one('payout/request').post(param).toPromise();
    return this.restangular.one('settlement/withdrawal').customPOST(param).toPromise();
  }

  getLogs(param): Promise<any> {
    // return this.restangular.one('payout/request').post(param).toPromise();
    return this.restangular.one('settlement').get(param).toPromise();
  }
}
