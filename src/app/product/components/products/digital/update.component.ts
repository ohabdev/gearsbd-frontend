import { Component, OnInit } from '@angular/core';
import { ToastyService } from 'ng2-toasty';
import { Router, ActivatedRoute } from '@angular/router';
import { LocationService, UtilService, AuthService } from '../../../../shared/services';
import { ProductService } from '../../../services/product.service';

@Component({
  selector: 'digital-product-create',
  templateUrl: 'update.component.html'
})
export class DigitalProductUpdateComponent implements OnInit {
  public digitalProducts : any;
  public boilerplateId : any;
  public productName : any = '';
  // public product: any = {
  //   digitalProductID : null,
  //   activationCode: '',
  //   visibility: 'private',
  //   price: 15,
  //   type: 'digital'
  // };
  // public product: any = {
  //   boilerplateId: null,
  //   name: '',
  //   description: '',
  //   specifications: [],
  //   mainImage: null,
  //   metaSeo: {
  //     keywords: '',
  //     description: ''
  //   },
  //   type: 'digital',
  //   categoryId: '',
  //   isActive: true,
  //   freeShip: false,
  //   featured: false,
  //   hot: false,
  //   bestSell: false,
  //   stockQuantity: 0,
  //   price: 0,
  //   salePrice: 0,
  //   vat: 0,
  //   restrictFreeShipAreas: [],
  //   restrictCODAreas: [],
  //   visibility: 'public',
  //   activationCode: [],
  //   // digitalFileId: '',
  //   // digitalFile: ''
  // };
  public product : any = {};
  public comissionFee  = 10;
  public digitalFee    = 5;
  public youMake       = 0;
  public isSubmitted: any = false;
  public accessToken: any = '';
  public isLoading = false;
  public newActivationCode: any = {
    value: ''
  };
  public page: any = 1;
  public take: any = 10;
  public total: any = 0;
  public searchText: any = '';
  public sortOption = {
    sortBy: 'createdAt',
    sortType: 'desc'
  };
  public items = [];
  public keys = [];
  public editKeyData: any = {};

  public fileType: any = '';
  public fileOptions: any = {};

  public tree: any = [];
  public brands: any = [];
  public newSpecification: any = {
    key: '',
    value: ''
  };
  public newProductOverview: any = [];
  public imageUrl: string = '';
  public images: any = [];
  public mainImage: string = '';
  public tab = 'info';
  public freeShipAreas: any = [];
  public countries: any = [];
  public states: any = [];
  public cities: any = [];
  public zipCode: any = '';
  public freeCountry: any;
  public freeState: any;
  public freeCity: any;
  public dealDate: any;
  public imagesOptions: any = {
    multiple: true
  };
  public isPluralNumberVal: any = true;
  public isNumberNull: any = true;

  constructor(
    private toasty: ToastyService,
    private productService: ProductService,
    private location: LocationService,
    private router: Router,
    private route: ActivatedRoute,
    private utilService: UtilService,
    private authService: AuthService
    ) {
      this.accessToken = this.authService.getAccessToken();
      const id = this.route.snapshot.paramMap.get('id');
      this.productService.findOne(id)
        .then(resp => {
          this.product = resp.data;
          console.log('Product', this.product);
          if(this.product){
            this.product.activationCode = [];
            this.boilerplateId = this.product.boilerplateId;
          }
          this.mainImage = resp.data.mainImage ? resp.data.mainImage._id : null;
          if (resp.data.digitalFile) {
            this.fileType = resp.data.digitalFile.type;
          }
          this.images = this.product.images;
          if (this.product.dealTo) {
            const date = new Date(this.product.dealTo);
            this.dealDate = {
              year: date.getFullYear(),
              month: date.getMonth() + 1,
              day: date.getDay()
            };
          }
        });
    }

  ngOnInit() {
    this.fileOptions = {
      multiple: false,
      url: window.appConfig.apiBaseUrl + '/media/files',
      onFinish: (resp) => {
        console.log(resp);
        if (resp.data.mimeType.indexOf('zip') > -1) {
          this.fileType = 'zip';
        } else if (resp.data.mimeType.indexOf('rar') > -1) {
          this.fileType = 'rar';
        } else if (resp.data.mimeType.indexOf('pdf') > -1) {
          this.fileType = 'pdf';
        } else {
          this.fileType = 'file';
        }
        this.product.digitalFileId = resp.data._id;
        this.product.digitalFile = resp.data;
      }
    };
    // this.digitalProducts = this.getProducts();
    this.query();
  }

  query() {
    this.utilService.setLoading(true);
    this.isLoading = true;
    this.productService.getBoilerplates({
      page: this.page,
      take: this.take,
      q: this.searchText,
      sort: `${this.sortOption.sortBy}`,
      sortType: `${this.sortOption.sortType}`
    })
      .then(resp => {
        this.items = resp.data.items;
        this.total = resp.data.count;
        this.searchText = '';
        this.utilService.setLoading(false);
        this.isLoading = false;
      }
      )
      .catch(() => {
        this.toasty.error('Something went wrong, please try again!');
        this.utilService.setLoading(false);
        this.isLoading = false;
      });

    const id = this.route.snapshot.paramMap.get('id');
    this.productService.getDigitalKeys(id)
    .then(resp => {
      console.log(resp);
      this.keys = resp.data.items;
      // this.total = resp.data.count;
      // this.searchText = '';
      // this.utilService.setLoading(false);
      // this.isLoading = false;
    })
    .catch(() => {
      this.toasty.error('Something went wrong, please try again!');
      this.utilService.setLoading(false);
      this.isLoading = false;
    });
  }

  calculateProfit($event) {
    const totalComission = this.comissionFee + this.digitalFee;
    if ( $event.target.value >= 15 ) {
      this.youMake = this.product.price - totalComission;
    }else {
      this.youMake = 0;
    }
  }

  submit(frm: any) {
    // console.log(this.product);
    // return false;
    this.isSubmitted = true;
    if (frm.invalid) {
      return this.toasty.error('Form is invalid, please try again.');
    }
    if(this.product.price < 1){
      return this.toasty.error('Price cannot be 0.');
    }

    // this.product.price = this.product.price;
    this.product.salePrice = this.product.salePrice;
    if(this.product.salePrice > this.product.price){
      return this.toasty.error('Product Price should be greater then Product Sale Price');
    }

    // if(this.product.activationCode.length < 1){
    //   return this.toasty.error('Enter Activation Code.');
    // }
    // if(!this.product.digitalFileId && !this.product.activationCode){
    //   return this.toasty.error('Please enter Activation Code or Add Digital File Path');
    // }
    // this.product.name = this.product.boilerplateId ? this.product.boilerplateId.name : '';
    this.product.name = '';
    this.items.forEach(item => {
      if(item._id == this.boilerplateId){
        console.log(item);
        this.product.name  = item.name;
        this.product.mainImage  = item.mediaId.id;
        this.product.images  = [item.mediaId.id];
      }
    }, this);
    this.product.salePrice = this.product.salePrice;
    // console.log(this.product);
    this.productService.update(this.product._id, this.product)
      .then(() => {
        this.toasty.success('Product has been created');
        this.router.navigate(['/products/digital/list']);
      }, err => this.toasty.error(err.data.message || 'Something went wrong!'));
  }

  searchProduct($event) {
    const searchQuery = $event.target.value;
      this.searchText = searchQuery;
      this.productService.getBoilerplates({
        page: this.page,
        take: this.take,
        q: this.searchText,
        sort: `${this.sortOption.sortBy}`,
        sortType: `${this.sortOption.sortType}`
      })
        .then(resp => {
          this.items = resp.data.items;
          this.total = resp.data.count;
          this.searchText = '';
        }
        )
        .catch(() => {
          this.toasty.error('Something went wrong, please try again!');
        });
  }

  addActivationCode() {
    if (!this.newActivationCode.value.trim()) {
      return this.toasty.error('Please enter Activation Code');
    }
    // console.log();
    this.product.activationCode.push(this.newActivationCode.value);
    this.newActivationCode = [];
  }
  removeActivationCode(index: number) {
    this.product.activationCode.splice(index, 1);
  }

  setProductName(item){
    this.product.name = item.name;
  }

  showModal(key):void {
    this.editKeyData = key;
    ($('#myModal') as any).modal('show');
  }

  hideModal():void {
    document.getElementById('close-modal').click();
  }

  updateKey(frm: any){
    console.log(frm.value);
    const data = frm.value;
    this.productService.updateKey(data.id, data)
      .then(() => {
        this.toasty.success('Key has been updated');
        this.hideModal();
      }, err => this.toasty.error(err.data.message || 'Something went wrong!'));
  }
}
