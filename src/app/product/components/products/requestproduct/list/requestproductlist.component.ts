import { Component, OnInit } from '@angular/core';
import { ProductService } from '../../../../services/product.service';
import { ToastyService } from 'ng2-toasty';
import { AuthService, UtilService } from '../../../../../shared/services';
import { Router } from '@angular/router';

@Component({
  selector: 'requestproduct-list',
  templateUrl: './requestproductlist.html',
  styleUrls: ['./requestproductlist.css']
})
export class RequestproductlistComponent implements OnInit {

  public items = [];
  public isLoading = false;
  public page: any = 1;
  public take: any = 10;
  public total: any = 0;
  constructor(
    private authService: AuthService,
    private router: Router,
    private productService: ProductService,
    private toasty: ToastyService,
    private utilService: UtilService
  ) { }

  ngOnInit() {
    this.query();
  }
  query() {
    this.utilService.setLoading(true);
    this.isLoading = true;

    this.productService.requestList({
      page: this.page,
      take: this.take
    })
      .then(resp => {
        this.items = resp.data.items;
        this.total = resp.data.count;
        console.log(resp);
        this.utilService.setLoading(false);
        this.isLoading = false;
      }
      )
      .catch(() => {
        this.toasty.error('Something went wrong, please try again!');
        this.utilService.setLoading(false);
        this.isLoading = false;
      });
  }

}
