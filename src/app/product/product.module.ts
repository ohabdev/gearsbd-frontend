import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SortablejsModule } from 'angular-sortablejs';
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';
import { ProductRoutingModule } from './product.routing';
import { NgSelectModule } from '@ng-select/ng-select';

import { VariantUpdateModalComponent, ProductVariantsComponent } from './components/variants/product-variants.component';
import { ProductCreateComponent } from './components/products/create.component';
import { ProductUpdateComponent } from './components/products/update.component';
import { PhysicalProductUpdateComponent } from './components/products/physical/update.component';
import { DigitalProductUpdateComponent } from './components/products/digital/update.component';
import { ProductsComponent } from './components/products/listing.component';

import { ProductCategoryService } from './services/category.service';
import { BrandService } from './services/brand.service';
import { OptionService } from './services/option.service';
import { ProductVariantService } from './services/variant.service';
import { ProductService } from './services/product.service';
import { LocationService } from '../shared/services';

import { MediaModule } from '../media/media.module';
import { ReviewModule } from '../review/review.module';
import { UserService } from '../user/user.service';
import { RequestproductComponent } from './components/products/requestproduct/request/requestproduct.component';
import { RequestproductlistComponent } from './components/products/requestproduct/list/requestproductlist.component'; 
import { DigitalProductCreateComponent } from './components/products/digital/create.component';
import { DigitalProductListComponent } from './components/products/digital/list.component';
import { PhysicalProductCreateComponent } from './components/products/physical/create.component';
import { PhysicalProductListComponent } from './components/products/physical/list.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SortablejsModule,
    // our custom module
    ProductRoutingModule,
    NgbModule.forRoot(),
    FroalaEditorModule.forRoot(),
    FroalaViewModule.forRoot(),
    MediaModule,
    ReviewModule,
    NgSelectModule
  ],
  declarations: [
    VariantUpdateModalComponent,
    ProductVariantsComponent,
    ProductCreateComponent,
    ProductUpdateComponent,
    PhysicalProductUpdateComponent,
    DigitalProductUpdateComponent,
    ProductsComponent,
    RequestproductComponent,
    RequestproductlistComponent,
    DigitalProductCreateComponent,
    DigitalProductListComponent,
    PhysicalProductCreateComponent,
    PhysicalProductListComponent
  ],
  providers: [
    ProductCategoryService,
    BrandService,
    UserService,
    OptionService,
    ProductVariantService,
    ProductService,
    LocationService
  ],
  entryComponents: [
    VariantUpdateModalComponent
  ]
})
export class ProductModule { }
