import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PhysicalProductUpdateComponent } from './components/products/physical/update.component';
import { DigitalProductUpdateComponent } from './components/products/digital/update.component';
import { ProductsComponent } from './components/products/listing.component';
import { RequestproductComponent } from './components/products/requestproduct/request/requestproduct.component';
import { RequestproductlistComponent } from './components/products/requestproduct/list/requestproductlist.component';
import { DigitalProductCreateComponent } from './components/products/digital/create.component';
import { DigitalProductListComponent } from './components/products/digital/list.component';
import { PhysicalProductCreateComponent } from './components/products/physical/create.component';
import { PhysicalProductListComponent } from './components/products/physical/list.component';

const routes: Routes = [
  {
    path: 'list',
    component: ProductsComponent,
    data: {
      title: 'Product manager',
      urls: [{ title: 'Products', url: '/products/list' }]
    }
  },
  // {
  //   path: 'create',
  //   component: ProductCreateComponent,
  //   data: {
  //     title: 'Product manager',
  //     urls: [{ title: 'Products', url: '/products/list' }, { title: 'Create', url: '/products/create' }]
  //   }
  // },
  {
    path: 'create',
    component: DigitalProductCreateComponent,
    data: {
      title: 'Product manager',
      urls: [{ title: 'Products', url: '/products/list' }, { title: 'Create', url: '/products/create' }]
    }
  },
  {
    path: 'digital/create',
    component: DigitalProductCreateComponent,
    data: {
      title: 'Digital Product manager',
      urls: [
        { title: 'Digital Products', url: '/products/digital/list' },
        { title: 'Create Digital Product', url: '/products/digital/create' }
      ]
    }
  },
  {
    path: 'digital/list',
    component: DigitalProductListComponent,
    data: {
      title: 'Digital Product manager',
      urls: [
        { title: 'Digital Products', url: '/products/digital/list' },
        { title: 'Create Digital Product', url: '/products/digital/create' }
      ]
    }
  },
  {
    path: 'physical/create',
    component: PhysicalProductCreateComponent,
    data: {
      title: 'Physical Product manager',
      urls: [
        { title: 'Physical Products', url: '/products/physical/list' },
        { title: 'Create Physical Product', url: '/products/physical/create' }
      ]
    }
  },
  { 
    path: 'physical/list',
    component: PhysicalProductListComponent,
    data: {
      title: 'Physical Product manager',
      urls: [
        { title: 'Physical Products', url: '/products/physical/list' },
        { title: 'Create Physical Product', url: '/products/physical/create' }
      ]
    }
  },
  {
    path: 'request/list',
    component: RequestproductlistComponent,
    data: {
      title: 'Product requrest manager',
      urls: [ { title: 'Product Requests', url: '/products/request/list' }]
    }
  },
  {
    path: 'request',
    component: RequestproductComponent,
    data: {
      title: 'Product requrest manager',
      urls: [{ title: 'Product Requests', url: '/products/request/list' }, { title: 'Request', url: '/products/request' }]
    }
  },
  {
    path: 'physical/update/:id',
    component: PhysicalProductUpdateComponent,
    data: {
      title: 'Product manager',
      urls: [{ title: 'Products', url: '/products/list' }, { title: 'Update', url: '/products/update' }]
    }
  },
  {
    path: 'digital/update/:id',
    component: DigitalProductUpdateComponent,
    data: {
      title: 'Product manager',
      urls: [{ title: 'Products', url: '/products/list' }, { title: 'Update', url: '/products/update' }]
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductRoutingModule { }
