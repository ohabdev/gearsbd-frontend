import { Injectable } from '@angular/core';
import { RestangularModule, Restangular } from 'ngx-restangular';
import 'rxjs/add/operator/toPromise';
import * as _ from 'lodash';

@Injectable()
export class ProductService {
  private allowFields = [
    'boilerplateId', 'activationCode',
    'name', 'alias', 'description', 'shortDescription', 'categoryId', 'brandId', 'specifications', 'overview',
    'chemicalIdentifiers', 'safetyHandling', 'isActive', 'metaSeo', 'ordering', 'freeShip',
    'images', 'mainImage', 'type', 'price', 'salePrice', 'stockQuantity', 'sku', 'upc', 'mpn', 'ean', 'digitalFileId',
    'jan', 'isbn', 'taxClass', 'taxPercentage', 'restrictCODAreas', 'restrictFreeShipAreas', 'dailyDeal', 'dealTo',
    'dangerousGoods', 'warranty', 'inTheBox', 'packageWeight', 'freeItems', 'approximateDeliveryTime'
  ];

  constructor(private restangular: Restangular) { }

  create(data: any): Promise<any> {
    return this.restangular.all('products').post(_.pick(data, this.allowFields)).toPromise();
  }

  search(params: any): Promise<any> {
    return this.restangular.one('products', 'search').get(params).toPromise();
  }

  searchPhysical(params: any): Promise<any> {
    return this.restangular.one('products/list', 'physical').get(params).toPromise();
  }

  searchDigital(params: any): Promise<any> {
    return this.restangular.one('products/list', 'digital').get(params).toPromise();
  }

  getItems(params: any): Promise<any> {
    return this.restangular.one('items').get(params).toPromise();
  }

  findOne(id): Promise<any> {
    return this.restangular.one('products', id).get().toPromise();
  }

  update(id, data): Promise<any> {
    return this.restangular.one('products', id).customPUT(_.pick(data, this.allowFields)).toPromise();
  }

  remove(id): Promise<any> {
    return this.restangular.one('products', id).customDELETE().toPromise();
  }

  export(params: any): Promise<any> {
    return this.restangular.one('products/export/csv').get(params).toPromise();
  }

  request(data: any): Promise<any> {
    return this.restangular.all('products/request').post(data).toPromise();
  }

  requestList(params: any): Promise<any> {
    return this.restangular.one('products/request/list').get(params).toPromise();
  }

  getBoilerplates(params: any): Promise<any> {
    return this.restangular.one('boilerplate', 'search').get(params).toPromise();
  }

  getDigitalKeys(id: any): Promise<any> {
    return this.restangular.one('products', id).one('keys').get().toPromise();
  }
  
  updateKey(id, data): Promise<any> {
    let key = _.omit(data,'id');
    return this.restangular.one('products/keys/', id).one('update').customPUT(key).toPromise();
  }

  activePackage(params: any): Promise<any> {
    return this.restangular.one('subscription/packages').get(params).toPromise();
  }

  boostProduct(boost: any): Promise<any> {
    return this.restangular.all('boosting/product').post(boost).toPromise();
    // return this.restangular.one('boosting/product').post(boost).toPromise();
  }

}
