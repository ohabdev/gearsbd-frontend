import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-profile-header',
  templateUrl: './profile-header.component.html'
})
export class ProfileHeaderComponent implements OnInit {
  @Input() user: any;
  constructor() { }

  ngOnInit() {
  }

}
